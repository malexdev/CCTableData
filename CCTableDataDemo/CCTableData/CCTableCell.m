//
//  CCTableCell.m
//  Debatable
//
//  Created by Alex Martin on 2/15/14.
//  Copyright (c) 2014 Code Chimera. All rights reserved.
//

#import "CCTableCell.h"

#define defaultReuseID @"cctablecellreuseid"

@implementation CCTableCell

+ (id)createWithTitle:(NSString *)title {
    CCTableCell *cell = [CCTableCell new];
    cell.cellFont = [UIFont systemFontOfSize:17.0];
    cell.title = title;
    cell.accessory = UITableViewCellAccessoryNone;
    cell.style = UITableViewCellStyleDefault;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.reuseID = defaultReuseID;
    cell.isMultilineCell = NO;
    return cell;
}

+ (id)createWithTitle:(NSString *)title andAccessory:(UITableViewCellAccessoryType)accessory selected:(void(^)(void))callback {
    CCTableCell *cell = [self createWithTitle:title];
    cell.accessory = accessory;
    cell.callbackSelect = callback;
    return cell;
}

+ (id)createWithTitle:(NSString *)title selected:(void(^)(void))callback {
    CCTableCell *cell = [self createWithTitle:title];
    cell.callbackSelect = callback;
    return cell;
}

+ (id)createWithTitle:(NSString *)title ofStyle:(UITableViewCellStyle)style selected:(void(^)(void))callback {
    CCTableCell *cell = [self createWithTitle:title];
    cell.style = style;
    cell.callbackSelect = callback;
    return cell;
}

+ (id)createWithTitle:(NSString *)title andSubtitle:(NSString *)subTitle selected:(void(^)(void))callback {
    CCTableCell *cell = [self createWithTitle:title];
    cell.subtitle = subTitle;
    cell.callbackSelect = callback;
    cell.style = UITableViewCellStyleSubtitle;
    return cell;
}

+ (id)createInputWithTitle:(NSString *)title secureText:(BOOL)secure textChanged:(void (^)(NSString *text, UITextField *input))callback {
    CCTableCell *cell = [self createWithTitle:title];
    cell.callbackInputTextDidEndEditing = callback;
    cell.isSecureTextInput = secure;
    cell.isInputCell = true;
    return cell;
}

+ (id)createInputWithTitle:(NSString *)title secureText:(BOOL)secure returnTapped:(void (^)(NSString *text, UITextField *input))callback {
    CCTableCell *cell = [self createWithTitle:title];
    cell.callbackInputReturnTapped = callback;
    cell.isSecureTextInput = secure;
    cell.isInputCell = true;
    return cell;
}

static float systemVersion = 0.0; // Querying the OS for its version is slow; cache the result the first time.
- (UITableViewCell *)createTableViewCellWithID:(NSString *)cellID onTableView:(UITableView *)table {
    if (systemVersion == 0.0) systemVersion = [[UIDevice currentDevice] systemVersion].floatValue; // Cache this; it's slow.
    
    if (self.isInputCell) {
        // Set up the input cell
        NSString *inputID = [cellID stringByAppendingString:@"-input"];
        UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:inputID];
        if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:inputID];
        
        //create text label field
        if (self.inputLabel == nil) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, cell.contentView.frame.size.width * 0.27, 20)];
            [label setBackgroundColor:[UIColor clearColor]];
            [label setFont:[UIFont systemFontOfSize:[UIFont smallSystemFontSize]]];
            [label setTextAlignment:NSTextAlignmentRight];
            [label setAdjustsFontSizeToFitWidth:true];
            [cell.contentView addSubview:label];
            [label setFrame:CGRectMake(label.frame.origin.x, ((cell.contentView.frame.size.height / 2) - (label.frame.size.height / 2)) - 1, label.frame.size.width, label.frame.size.height)];
            self.inputLabel = label;
        }
        
        //create text input field
        if (self.inputField == nil) {
            UITextField *text = [[UITextField alloc] initWithFrame:CGRectMake(cell.contentView.frame.size.width * 0.3, 5, cell.contentView.frame.size.width - (cell.contentView.frame.size.width * 0.3), cell.contentView.frame.size.height - 10)];
            [text setAdjustsFontSizeToFitWidth:true];
            [text setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
            [text setFont:[UIFont systemFontOfSize:[UIFont smallSystemFontSize]]];
            [text setBorderStyle:UITextBorderStyleNone];
            [text setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
            [text setDelegate:self];
            [text setClearButtonMode:UITextFieldViewModeWhileEditing];
            [cell.contentView addSubview:text];
            self.inputField = text;
        }
        
        // set input label options
        [self.inputLabel setText:self.title];
        
        //apply options to the input field
        [self.inputField setSecureTextEntry:self.isSecureTextInput];
        [self.inputField setText:self.initialInputValue];
        [self.inputField setPlaceholder:self.subtitle];
        [self.inputField setKeyboardType:self.keyboardType];
        [self.inputField setAutocorrectionType:self.autocorrectionType];
        [self.inputField setAutocapitalizationType:self.autocapitalizationType];
        
        return cell;
    } else if (self.isMultilineCell) {
        NSString *multiID = [cellID stringByAppendingString:@"-multiline"];
        UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:multiID];
        if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:multiID];

        cell.textLabel.text = self.title;
        cell.textLabel.font = self.cellFont;
        cell.textLabel.numberOfLines = 0;
        cell.selectionStyle = self.selectionStyle;
        cell.accessoryType = self.accessory;

        // If this is a UITableViewVellValue2 and we're on iOS 7, set the tint color to match the system
        if (self.style == UITableViewCellStyleValue2) {
            if (systemVersion >= 7.0) {
                UIColor *globalTint = [UIView appearance].tintColor;
                cell.textLabel.textColor = globalTint;
            }
        }

        return cell;
    } else {
        UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:cellID];
        if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:self.style reuseIdentifier:cellID];
        
        cell.textLabel.text = self.title;
        cell.textLabel.font = self.cellFont;
        cell.detailTextLabel.text = self.subtitle;
        cell.selectionStyle = self.selectionStyle;
        cell.accessoryType = self.accessory;
        
        // If this is a UITableViewVellValue2 and we're on iOS 7, set the tint color to match the system
        if (self.style == UITableViewCellStyleValue2) {
            if (systemVersion >= 7.0) {
                UIColor *globalTint = [UIView appearance].tintColor;
                cell.textLabel.textColor = globalTint;
            }
        }
        
        return cell;
    }
}

#pragma mark - UITextField delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (self.callbackInputTextDidBeginEditing != nil) self.callbackInputTextDidBeginEditing(textField.text, textField);
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (self.callbackInputTextDidEndEditing != nil) self.callbackInputTextDidEndEditing(textField.text, textField);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.callbackInputReturnTapped != nil) self.callbackInputReturnTapped(textField.text, textField);
    return true;
}

@end
